import React, { useRef, useEffect, useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from 'axios';
import { Navbar, Container, Nav } from 'react-bootstrap';
import './App.css';
import Home from './components/Home';
import Add from './components/Add';
import Run from './components/Run';
import Res from './components/Res';

function App() {
  const isMounted = useRef(null);

  const [user, setUser] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [mostRecentAdd, setMostRecentAdd] = useState(null);
  const [mostRecentRun, setMostRecentRun] = useState(null);

  useEffect(() => {
    isMounted.current = true;
    // FIXME addresses the window not inserting the correct has address sometimes
    window.location.href = `${window.location.origin}/?#`;
    // eslint-disable-next-line
    return () => (isMounted.current = false);
  }, []);

  useEffect(() => {
    setIsLoading(true);
    axios({
      baseURL: 'http://localhost:5000',
      method: 'get',
      url: 'api/auth/currentUser',
    }).then((res) => {
      if (isMounted.current) {
        setUser(res.data.user || {});
        // make these into dicts for easier lookup
        setIsLoading(false);
      }
    });
  }, []);

  return (
    <div className="App">
      <Navbar bg="dark" variant="dark" expand="lg" sticky="top">
        <Container>
          <Navbar.Brand href="#">people reign</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto" navbarScroll>
              <Nav.Link href="#home">Home</Nav.Link>
              <Nav.Link href="#instructions">HowTo</Nav.Link>
              <Nav.Link href="#add">Add</Nav.Link>
              <Nav.Link href="#run">Run</Nav.Link>
              <Nav.Link href="#results">Results</Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      <Container className="p4 mt4">
        <Home name="home" />
        <Add
          name="add"
          isLoading={isLoading}
          setIsLoading={setIsLoading}
          setMostRecent={setMostRecentAdd}
        />
        <Run
          name="run"
          isLoading={isLoading}
          setIsLoading={setIsLoading}
          mostRecentAdd={mostRecentAdd}
          setMostRecentRun={setMostRecentRun}
        />
        <Res
          name="results"
          isLoading={isLoading}
          setIsLoading={setIsLoading}
          mostRecentRun={mostRecentRun}
        />
      </Container>
    </div>
  );
}

export default App;
