import React from 'react';
import './Home.css';
import add from '../static/add.png';
import res from '../static/res.png';
import run from '../static/run.png';

const Collection = () => (
  <div className="Home--Parent">
    <h2 id="home"> Home </h2>
    <div>
      <h4> MVP Checklist </h4>

      <div>The resulting MVP must:</div>
      <ul>
        <li>
          Be able to accept a request and construct a response conforming to the API schema defined
          in the Code section below
        </li>
        <li>
          Set values defined in the object under the ctx key of the request to a global variable
          called ctx before executing the script
          <ul>
            <li>
              Example: if the request includes ctx.firstName = “John”, then set ctx.firstName to be
              a global variable in the script-execution environment before executing the text of the
              script so that if the script-text references the variable ctx.firstName, it takes the
              value “John”.
            </li>
          </ul>
        </li>

        <li>
          Set res to be a global variable, defaulting to an empty object, before executing the
          script.
          <ul>
            <li>
              When the script completes, include the values for the object res in the response under
              the key “res”.
            </li>
          </ul>
        </li>
        <li>
          Be able to execute the Javascript script provided as string-text under the script key
        </li>
        <li>
          Handle errors/exceptions in script-executions by providing a response “success”: false.
        </li>
      </ul>
    </div>
    <div />
    <div>
      <h4>Roadmap/Improvements </h4>
      <ul>
        <li> navbar hash tracking - update the navbar display to reflect scrolling</li>
        <li> sandbox executable code in a better way</li>
        <ul>
          <li>
            currently, code is executed on a standalone express server, inside a node vm, seperated
            from the platform db and server
          </li>
          <li>
            this is not super safe, but allows for easy abstraction down the road to use k8
            containers and better sandboxing
          </li>
          <li>
            also allows easy abstraction for multi-threading for more intense processes and scripts
          </li>
        </ul>
        <li>
          add timestamp to run history, this will provide more information to the user about
          previous script runs
        </li>
        <li> add name field to code table and input, give option to user to name scripts</li>
        <ul>
          <li>the name will be nullable and non distinct</li>
          <li>
            will not be used for db or code indentification - only as a identifier/visual-sugar for
            the end-user
          </li>
        </ul>
        <li> show a last run component</li>
        <ul>
          <li> pretty format json display here</li>
          <li> have button to quickly re-run script or discard run</li>
          <li> place above the run history</li>
        </ul>
        <li>
          create user authorization flow and ability to share scripts and results based on user
          groups
        </li>
        <li> finish method where the user can delete scripts and previous runs</li>
        <li> implement method for user to directly run script without saving it first</li>
        <ul>
          <li>
            Save the script to DB during run step - so skip the save and input step and go to last
            run component (to be made)
          </li>
        </ul>
        <li> paginate lastRun endpoints to be able to retrieve more granular list</li>
        <li> pretty format JSON responses on frontend for display</li>
        <li> Implement error and warning features without silently failing</li>
        <li>
          Make hash nav only scroll to halway down page instead of putting header at top - make it
          less jarring
        </li>
      </ul>
    </div>
    <h4 id="instructions"> Useage </h4>
    <p> Below are basic instructions to get started creating executable scripts.</p>
    <ol>
      <li>
        First create a script in the Add Scripts section below.
        <ul>
          <li>Reserved words are `ctx` and `res`</li>
          <li>
            ctx is used with a post-script to signify variable names, for example `ctx.firstName`
            specifies a variable `firstName`
          </li>
          <li>
            res contains any results that need to be stored or communicated back to the user. An
            example would be `res.message`, where `message` is being assigned to a variable value as
            seen below.
          </li>
        </ul>
        <img src={add} alt="Add_image" style={{ width: '70%' }} />
      </li>
      <li> Next, save the script by pressing the Submit button in the `Add Scripts` section. </li>
      <li>
        Run the saved script, by selecting the script in the `Run Scripts` section
        <ul>
          <li>
            Notice the section at the bottom that contains the variable names as defined previously.
          </li>
          <li>The inputs here are used to provide variable values.</li>
          <li>Pressing Clear will clear the variable input fields.</li>
          <li>
            The `-` and `+` signs are to manually remove or add ctx variables that exist in the
            script.
          </li>
          <li>After the variables are filled out, press the run button to continue.</li>
        </ul>
        <img src={run} alt="Run_image" style={{ width: '70%' }} />
      </li>
      <li>
        View the results in the `Results` section
        <ul>
          <li>The most recent results are highlighted.</li>
        </ul>
        <img src={res} alt="Result_image" style={{ width: '70%' }} />
      </li>
    </ol>

  </div>
);

export default Collection;
