import React from 'react';
import { Form, Button } from 'react-bootstrap';
import axios from 'axios';
import Variable from './Variable';
import TabbedInterface from './TabbedInterface';
import './Run.css';

const Run = ({ isLoading, setIsLoading, mostRecentAdd, setMostRecentRun }) => {
  const isMounted = React.useRef(null);

  React.useEffect(() => {
    isMounted.current = true;
    // eslint-disable-next-line
    return () => (isMounted.current = false);
  }, []);

  const [variables, setVariables] = React.useState([]);
  const [count, setCount] = React.useState(0);
  const [varToRemove, setVarToRemove] = React.useState(null);
  const [scripts, setScripts] = React.useState(null);
  const [selected, setSelected] = React.useState(null);
  const [oldSelected, setOldSelected] = React.useState('');

  const removeVariableInput = React.useCallback(
    (id) => {
      setVarToRemove(id);
    },
    [setVarToRemove]
  );

  const addVariableInput = React.useCallback(() => {
    const r = [...variables];
    const select = scripts[scripts.findIndex((s) => s.script_id === selected)];
    const vars = select.script.match(/(ctx.[a-z]*)+/gi);
    let firstMissing = null;
    for (let i = 0; i < vars.length; i += 1) {
      const find = r.findIndex((v) => v.props.label === vars[i]);
      if (find === -1) {
        firstMissing = i;
        break;
      }
    }
    if (vars.length > count && firstMissing > -1) {
      const ref = React.createRef();
      r.push(
        <Variable
          key={`variable_input_${firstMissing}`}
          removeVariableInput={() => removeVariableInput(`variable_input_${firstMissing}`)}
          ref={ref}
          label={vars[firstMissing]}
        />
      );
      setCount(r.length);
      setVariables(r);
    }
  }, [variables, count]);

  React.useEffect(() => {
    setIsLoading(true);
    axios({
      baseURL: 'http://localhost:5000',
      method: 'get',
      url: 'api/listScripts',
    }).then((res) => {
      if (isMounted.current) {
        setIsLoading(false);
        if (res.status === 200) {
          setScripts(res.data.data);
          if (res.data.data[res.data.data.length - 1].script === mostRecentAdd) {
            setSelected(res.data.data.length);
          }
        }
      }
    });
  }, [mostRecentAdd]);

  const submit = React.useCallback(() => {
    setIsLoading(true);
    const ctx = variables.reduce(
      (pre, cur) => ({ ...pre, [cur.props.label.replace('ctx.', '')]: cur.ref.current.value }),
      {}
    );
    axios({
      baseURL: 'http://localhost:5000',
      method: 'post',
      url: 'api/runScript',
      data: { scriptId: selected, ctx },
    }).then((res) => {
      if (isMounted.current) {
        setIsLoading(false);
        if (res.status === 200) {
          setMostRecentRun({ scriptId: selected, ctx });
          window.location.hash = '#results';
        }
      }
    });
  }, [variables]);

  const cancel = React.useCallback(() => {
    setVariables([]);
    setCount(0);
  }, [variables]);

  React.useEffect(() => {
    if (varToRemove) {
      const t = [...variables];
      const i = t.findIndex((v) => v.key === varToRemove);
      t.splice(i, 1);
      setVariables(t);
      setVarToRemove(null);
      setCount(t.length);
    }
  }, [varToRemove, setVarToRemove, variables]);

  React.useEffect(() => {
    if (selected && oldSelected !== selected && scripts.length > 0) {
      cancel();
      const r = [];
      const select = scripts[scripts.findIndex((s) => s.script_id === selected)];
      const vars = select.script.match(/(ctx.[a-z]*)+/gi);
      for (let i = 0; i < vars.length; i += 1) {
        const ref = React.createRef();
        r.push(
          <Variable
            key={`variable_input_${i}`}
            removeVariableInput={() => removeVariableInput(`variable_input_${i}`)}
            ref={ref}
            label={vars[i]}
          />
        );
      }
      setCount(r.length);
      setVariables(r);
      setOldSelected(selected);
    }
  }, [selected, scripts, count]);

  return (
    <div className="run--parent">
      <h2 id="run"> Run Scripts</h2>
      {scripts && (
        <Form>
          <Form.Group className="mb-3" controlId="formAddScript.textarea">
            <Form.Label>Script</Form.Label>
            <Form.Control
              as={TabbedInterface}
              selected={selected}
              setSelected={setSelected}
              data={scripts}
            />
          </Form.Group>
          <Form.Group className="mb-3 w-90" controlId="formAddScript.input">
            <Form.Label className="var--header">
              <span className="float-start">Variables</span>
              <Button className="float-end" onClick={addVariableInput}>
                +
              </Button>
            </Form.Label>
            {variables}
          </Form.Group>

          <Form.Group className="float-end" controlId="formAddScript.buttons">
            <Button className="mx-3" variant="danger" onClick={cancel}>
              Clear
            </Button>
            <Button className="mx-3" type="submit" variant="success" onClick={submit}>
              Run
            </Button>
          </Form.Group>
        </Form>
      )}
      {!scripts && <div>No scripts to run, add one!</div>}
    </div>
  );
};
export default Run;
