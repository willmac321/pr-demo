import React from 'react';
import { Form, Button } from 'react-bootstrap';
import './Variable.css';

const Variable = React.forwardRef(({ removeVariableInput, label }, ref) => (
  <div className="var-input--wrapper">
    <span className="var-input--input-label">
      <Form.Label>{label}</Form.Label>
    </span>
    <span className="var-input--input">
      <Form.Control
        key={`form_input--${label}`}
        as="input"
        type="string"
        placeholder="Variable name"
        ref={ref}
      />
    </span>
    <Button variant="secondary" className="float-end" onClick={removeVariableInput}>
      -
    </Button>
  </div>
));

export default Variable;
