import React from 'react';
import axios from 'axios';
import BootstrapTable from 'react-bootstrap-table-next';
import { Table } from 'react-bootstrap';
import './Res.css';

const Res = ({ isLoading, setIsLoading, mostRecentRun }) => {
  const isMounted = React.useRef(null);
  const [history, setHistory] = React.useState([]);

  const columns = [
    {
      dataField: 'id',
      text: 'ID',
      sort: true,
      style: { wordBreak: 'break-all' },
    },
    {
      dataField: 'code',
      text: 'Script',
      sort: true,
      style: { wordBreak: 'break-all' },
    },
    {
      dataField: 'result',
      text: 'Result',
      sort: true,
      style: { wordBreak: 'break-all', whiteSpace: 'pre-line' },
    },
    {
      dataField: 'ran_by',
      text: 'Ran By',
      sort: true,
      style: { wordBreak: 'break-all' },
    },
  ];

  const selectRow = {
    mode: 'radio',
    clickToSelect: true,
    selected: [history.length],
    style: { background: 'lightgrey' },
  };

  React.useEffect(() => {
    isMounted.current = true;
    // eslint-disable-next-line
    return () => (isMounted.current = false);
  }, []);

  React.useEffect(() => {
    setIsLoading(true);
    axios({
      baseURL: 'http://localhost:5000',
      method: 'get',
      url: 'api/scriptHistory',
    }).then((res) => {
      if (isMounted.current) {
        setIsLoading(false);
        if (res.status === 200) {
          let result = res.data.data.reverse();
          result = result.map((r) => ({
            ...r,
            result: JSON.stringify(JSON.parse(r.result, null, '\t')),
          }));
          setHistory(result);
        }
      }
    });
  }, [mostRecentRun]);

  return (
    <div className="res--parent">
      <h2 id="results"> Results</h2>
      <BootstrapTable
        noDataIndication={() => <div>No results to display.</div>}
        keyField="id"
        data={history}
        columns={columns}
        selectRow={selectRow}
        hover
        bodyStyle={{ width: 100, maxWidth: 100, wordBreak: 'break-all' }}
      />
    </div>
  );
};
export default Res;
