import React from 'react';
import { Form, Button } from 'react-bootstrap';
import axios from 'axios';
import './Add.css';

const Add = ({ isLoading, setIsLoading, setMostRecent }) => {
  const placeholder = 'Create a script, use ctx.[variableName] to specify variables.';

  const isMounted = React.useRef(null);

  React.useEffect(() => {
    isMounted.current = true;
    // eslint-disable-next-line
    return () => (isMounted.current = false);
  }, []);

  const textAreaRef = React.useRef();

  const submit = React.useCallback(() => {
    if (textAreaRef.current.value) {
      setIsLoading(true);
      axios({
        baseURL: 'http://localhost:5000',
        method: 'post',
        url: 'api/setCode',
        data: { script: textAreaRef.current.value, type: 'string' },
      }).then((res) => {
        if (isMounted.current) {
          setIsLoading(false);
          if (res.status === 200) {
            setMostRecent('');
            setMostRecent(textAreaRef.current.value);
            window.location.hash = '#run';
          }
        }
      });
    }
  }, []);

  const cancel = React.useCallback(() => {
    textAreaRef.current.value = '';
  }, []);

  return (
    <div className="add--parent">
      <h2 id="add"> Add Scripts</h2>
      <Form>
        <Form.Group className="mb-3" controlId="formAddScript.textarea">
          <Form.Label>Script</Form.Label>
          <Form.Control ref={textAreaRef} as="textarea" rows={5} placeholder={placeholder} />
        </Form.Group>

        <Form.Group className="float-end" controlId="formAddScript.buttons">
          <Button className="mx-3" variant="danger" onClick={cancel}>
            Clear
          </Button>
          <Button className="mx-3" type="submit" onClick={submit}>
            Submit
          </Button>
        </Form.Group>
      </Form>
    </div>
  );
};

export default Add;
