import React from 'react';
import { Tab, Row, Col, ListGroup } from 'react-bootstrap';
import './TabbedInterface.css';

const TabbedInterface = ({ selected, setSelected, data }) => {
  const setLocalSelected = (v) => {
    setSelected(v);
  };

  return (
    <Tab.Container id="list-group-tabs-example" activeKey={`#link${selected}`}>
      {data && (
        <Row>
          <Col sm={4}>
            <ListGroup className="scripts--listGroup-parent">
              {data.map((v) => (
                <ListGroup.Item
                  action
                  key={`tab_script_${v.script_id}`}
                  href={`#link${v.script_id}`}
                  onClick={() => setLocalSelected(v.script_id)}
                  className="scripts--listGroup"
                >
                  <span>
                    {v.script_id} __ {v.script}
                  </span>
                </ListGroup.Item>
              ))}
            </ListGroup>
          </Col>
          <Col sm={8}>
            <Tab.Content>
              {data.map((v) => (
                <Tab.Pane
                  key={`tab_content_${v.script_id}`}
                  className="scripts--tabbedPane"
                  eventKey={`#link${v.script_id}`}
                >
                  <div>{v.script}</div>
                </Tab.Pane>
              ))}
            </Tab.Content>
          </Col>
        </Row>
      )}
    </Tab.Container>
  );
};

export default TabbedInterface;
