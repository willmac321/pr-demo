const express = require('express')
const vm = require('vm');
const app = express()
const port = 8686

app.use(express.json());

app.post('/run', (req, result) => {
  const data = req.body

  if (!data){
    result.sendStatus({code: 400});
  }

  let res = {};

  let context = {ctx:data.ctx, res:res};

// use node VM to run code;
  // as noted in docs, dont use it run untrusted code
// hence, abstracting the execution env outside of backend server - this can be moved to a seperate sandboxed env later on
  vm.createContext(context);

  vm.runInContext(data.script.replace('\\n', ';'), context);

  result.send(res)
})

app.put('/cancel', (req, res) => {
  // TODO add method that will cancel all running scripts
  res.sendStatus(200);
})

app.listen(port, () => {
  console.log(`Exec env listening on port ${port}`)
})
