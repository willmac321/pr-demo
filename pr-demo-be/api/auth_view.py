from flask import Blueprint, request

from api.db import get_db

auth_view = Blueprint("auth_view", __name__)

name = "Will"
currentUserId = 1

# TODO login flow goes here
# TODO If this were in prod - would use auth package to get current logged in user
@auth_view.route("/api/auth/currentUser", methods=["GET"])
def get_user():
    """Get user by ID

    Return user as specified in query `currentUserId`, if query argument not supplied, use current user
    """
    arg_user = request.args.get("nameId", default=currentUserId)
    db = get_db()
    user = db.execute("SELECT * FROM users WHERE id = ?", (arg_user,)).fetchone()
    return {"id": user["id"], "name": user["name"]}


