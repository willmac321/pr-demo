import json
import os

import requests
from flask import Blueprint, abort, jsonify, render_template, request
from flask_cors import cross_origin

from api.auth_view import currentUserId
from api.db import get_db

asset_view = Blueprint("asset_view", __name__)


EXEC_ENV = os.getenv("EXEC_ENV_ADDR")


@asset_view.route("/api/listScripts", methods=["GET"])
def get_user_scripts():
    """Get user scripts by ID

    Return array of user scripts by query `currentUserId` if query argument is not supplied, use current user
    """
    arg_user = request.args.get("nameId", default=currentUserId)
    db = get_db()

    # TODO use auth environment to get current logged in user
    # TODO will a user be able to execute a different user's scripts?
    user = db.execute("SELECT * FROM users WHERE id = ?", (arg_user,)).fetchone()

    if user:
        results = db.execute(
            """SELECT c.code_str as scripts, c.id as code_id, c.type, u.name as user
            FROM code c inner join users u on u.id = c.owner_id
            where u.id = ?""",
            (int(user["id"]),),
        ).fetchall()
        rv = []
        for item in results:
            rv.append(
                {
                    "script_id": item["code_id"],
                    "script": item["scripts"],
                    "user": item["user"],
                }
            )

        return {"data": rv}
    abort(400)


@asset_view.route("/api/runScript", methods=["POST"])
def run_user_script():
    """Run user script by ID

    Run user script as specified by arg scriptId, if successful, return the value specified in script, else return error
    """
    request_data = request.get_json()
    if request_data is None:
        abort(400)

    script_id = request_data["scriptId"]
    ctx = request_data["ctx"]

    # Bail if no script or ctx is None, send an empty object is not ctx
    if script_id is None or ctx is None:
        abort(400)

    db = get_db()

    # TODO use auth environment to get current logged in user
    # TODO will a user be able to execute a different user's scripts?
    script = db.execute(
        "SELECT code_str as script FROM code WHERE owner_id = ? and id = ?",
        (currentUserId, script_id),
    ).fetchone()

    if script:
        script_data = {"script": script["script"], "ctx": ctx}

        headers = {"content-type": "application/json"}
        resp = requests.post(
            f"{EXEC_ENV}/run", headers=headers, data=json.dumps(script_data)
        )

        ## TODO add timestamp for run
        db.execute(
            "INSERT or ignore INTO code_run_results (result, code_id, user_id) values (?,?,?)",
            (resp.text, script_id, currentUserId),
        )
        db.commit()

        return (resp.content, resp.status_code, resp.headers.items())
    abort(400)


@asset_view.route("/api/scriptHistory", methods=["GET"])
def get_past_runs():
    """Get script run history for logged in user

    Return the results for previous script runs for the logged in user
    """
    # TODO paginate
    db = get_db()
    results = db.execute(
        """
        SELECT crr.id, crr.code_id as code_id, crr.result, c.code_str as code, u.name as ran_by
        FROM code_run_results as crr
        inner join code c on c.id = crr.code_id
        inner join users u on u.id = crr.user_id where u.id = ? """,
        (currentUserId,),
    ).fetchall()

    rv = []
    for item in results:
        rv.append(
            {
                "id": item["id"],
                "code_id": item["code_id"],
                "result": item["result"],
                "code": item["code"],
                "ran_by": item["ran_by"],
            }
        )

    return {"data": rv}


@asset_view.route("/api/setCode", methods=["POST"])
@cross_origin()
def add_code():
    """add script to db

    Adds a script to the database to be executed later
    """
    request_data = request.get_json()
    script = request_data["script"]
    script_type = request_data["type"]

    db = get_db()

    # TODO user would use auth instead of hardcoded
    # TODO do malicious script check here
    db.execute(
        "INSERT or ignore INTO code (type, code_str, owner_id) values (?,?,?)",
        (script_type, script, currentUserId),
    )
    db.commit()
    return jsonify(success=True)


@asset_view.route("/api/removeCode", methods=["DELETE"])
@cross_origin()
def remove_code():
    """remove script from db

    Removes script from DB
    """
    request_data = request.get_json()
    script_id = request_data["scriptId"]

    db = get_db()

    # TODO user would use auth instead of hardcoded

    # Can only delete 'owned' scripts, in future may need to have a client_org/admin classifier
    db.execute(
        "DELETE FROM code where id = ? and owner_id = ?",
        (script_id, currentUserId),
    )
    db.commit()
    return jsonify(success=True)


# FIXME If this were in prod - this wouldn't exist or have restricted access
@asset_view.route("/api/showall", methods=["GET"])
def get_showall():
    db = get_db()
    scripts = db.execute("SELECT * FROM code").fetchall()
    return render_template("dump_data.html", results=scripts)
