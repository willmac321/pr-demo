import sqlite3

import click
from flask import current_app, g
from flask.cli import with_appcontext


def init_app(app):
    app.teardown_appcontext(close_db)
    app.cli.add_command(init_db_command)
    app.cli.add_command(clear_db_command)


def get_db():
    if "db" not in g:
        g.db = sqlite3.connect(
            current_app.config["DATABASE"], detect_types=sqlite3.PARSE_DECLTYPES
        )
        g.db.row_factory = sqlite3.Row

    return g.db


def close_db(e=None):
    db = g.pop("db", None)

    if db is not None:
        db.close()


def init_db():
    db = get_db()
    with current_app.open_resource("schema_factory.sql") as f:
        db.executescript(f.read().decode("utf8"))


def clear_db():
    db = get_db()
    with current_app.open_resource("schema_factory_reset.sql") as f:
        db.executescript(f.read().decode("utf8"))
    init_db()


@click.command("init-db")
@with_appcontext
def init_db_command():
    """Create new tables if don't exist."""
    init_db()
    click.echo("Initialized the database.")


@click.command("remake-db")
@with_appcontext
def clear_db_command():
    """Drop and Create new tables if don't exist."""
    clear_db()
    click.echo("Cleared and reinitiliazed the database.")
