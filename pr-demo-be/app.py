import os
from flask import Flask
from flask_cors import CORS
from dotenv import load_dotenv
from api import db
from api.asset_view import asset_view
from api.auth_view import auth_view

load_dotenv()

app = Flask(__name__)
CORS(app)

app.config["DATABASE"] = os.getenv("DATABASE")

db.init_app(app)

app.register_blueprint(asset_view)
app.register_blueprint(auth_view)
