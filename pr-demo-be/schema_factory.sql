CREATE TABLE IF NOT EXISTS users (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS code(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  type TEXT,
  code_str TEXT,
  owner_id TEXT,
  FOREIGN KEY(owner_id) REFERENCES users(id)
);

Create TABLE IF NOT EXISTS code_run_results (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  result TEXT,
  code_id INTEGER,
  user_id INTEGER,
  FOREIGN KEY(code_id) REFERENCES code(id)
  FOREIGN KEY(user_id) REFERENCES users(id)
);

INSERT INTO users(name) VALUES ('Will');

INSERT INTO code(type, code_str, owner_id) VALUES ("string", "let msg = 'Hello ' + ctx.firstName + '!'"||char(10)||"res.message=msg", 1);
