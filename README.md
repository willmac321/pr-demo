# PeopleReign

TODO
- navbar hash tracking
- sandbox executable code in a better way
- multi-thread or k8 for exec code
- add timestamp to run history
- show a last run component
 - place above the run history
- implement method for user to directly run script without saving it first
 - Save the script to DB during run step - so skip the save and input step and go to last run component (to be made)
- paginate lastRun endpoints to be able to retrieve more granular list
- pretty format JSON responses on frontend for display
- Implement error and warning features without silently failing
- Make hash nave only scroll to halway down page instead of putting header at top - make it less jarring


# Production Setup (docker deploy)

The production environment uses docker-compose and the docker-compose.yml file at project root to run the platform.

Please make sure that `Docker` and `Docker-compose` are installed on your system:
### Docker install
```
# for ubuntu
https://docs.docker.com/engine/install/ubuntu/

# for mac & windows
https://docs.docker.com/get-docker/

```

### Docker-Compose install
```
# follow platform specific instructions here
https://docs.docker.com/compose/install/
```

!!! make sure that the current user has the ability to run docker & docker-compose !!!


### Run the Production App
```
# in a terminal at the project root (the same directory that contains docker-compose.yml) run
docker-compose build
docker-compose up
```

Then navigate to `localhost:3000` to use the platform.

# Development Setup
## FE
use yarn for FE

### init FE
```
yarn
yarn run
```
## BE execution environment
the BE uses a seperate express server for code execution

### init BE-exec and run dev
```
yarn
yarn start
```

## BE
The BE uses flask and the builtin sqlite3 for a db

### init BE
copy .env.example to .env
```
cp .env.example .env
```
make sure to uncomment the localhost line if doing local dev
if deploying on docker, use the exec_env line as the `EXEC_ENV_ADDR`

```
# create python venv, I used 3.10.2 at time of making this and pyenv
pyenv install 3.10.2
pyenv local 3.10.2

# check version
python --version

python -m venv .venv
```

#### To activate and install dependencies

```
source .venv/bin/activate
pip install -r requirements.txt
```

#### To reset database

```
flask remake-db
```

#### To init database

```
flask init-db
```

#### To run dev server

```
flask run
```
